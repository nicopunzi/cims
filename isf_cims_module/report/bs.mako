<html>
<head>
    <style type="text/css">
        ${css}
        
        .currency_border {
		    width: "10%";
		    text-align: right;
		    white-space: nowrap;
		    border: 1px solid black;
		}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <center>
    <h1><b><i>Reconciliation Statement</i></b></h1>
    </center>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headerlines()
    print lines()
    print chequelines()
    """
    %>
%for o in objects:
    <table style="width: 100%;" border="0">
        <tr>
            <td>Statement :</td>
            <td>${o.name} </td>
        </tr>
        <tr>
            <td>Account :</td>
            <td>${o.account_id.name}</td>
        </tr>
        <tr>
            <td>Period :</td>
            <td>${o.period_id.name}</td>
        </tr>
        <tr>
            <td>Status :</td>
            <td><b>${o.state}</b></td>
        </tr>
    </table>

    <table style="width: 100%;" border="0">
        <tr class="row">
                <td colspan="2">
                    <table class="tr_bottom_line"></table>
                </td>
            </tr>
        <tr class="table_header">
            <td> Description </td>
            <td class="currency"> Amount </td>
        </tr>
        <tr class="row">
            <td colspan="2">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
        <tr>
            <td>- Uncleared Previous Period</td>
            <td class="currency">${o.uncleared_prev_period}</td>
        </tr>
        <tr>
            <td><b>A. Opening Book Balance</b></td>
            <td class="currency"><b>${o.starting_balance}</b></td>
        </tr>
        <tr>
            <td>B. Cleared Receipts</td>
            <td class="currency">${o.cleared_receipts}</td>
        </tr>
        <tr>
            <td>C. Uncleared Receipts</td>
            <td class="currency">${o.uncleared_receipts}</td>
        </tr>
        <tr>
            <td><b>D. Total Receipts (B + C)</b></td>
            <td class="currency"><b>${formatLang(o.cleared_receipts + o.uncleared_receipts)}</b></td>
        </tr>
        <tr>
            <td>E. Cleared Payments</td>
            <td class="currency">${formatLang(-o.cleared_payments)}</td>
        </tr>
        <tr>
            <td>F. Uncleared Payments</td>
            <td class="currency">${formatLang(-o.difference)}</td>
        </tr>
        <tr>
            <td><b>G. Total Payments (E + F)</b></td>
            <td class="currency"><b>${formatLang(-o.cleared_payments - o.difference)}</b></td>
        </tr>
        <tr>
            <td><b>H. Ending Book Balance (A + D - G)</b></td>
            <td class="currency"><b>${formatLang(o.total_amount)}</b></td>
        </tr>
        <tr>
            <td><b>I. Bank Statement Balance</b></td>
            <td class="currency_border"><b>${o.ending_balance}</b></td>
        </tr>
        <tr>
            <td><b>J. Difference (H - I)</b></td>
            <td class="currency"><b>${formatLang(o.total_amount - o.ending_balance)}</b></td>
        </tr>    
        <tr class="row">
            <td colspan="2">
                <table class="tr_bottom_line"></table>
            </td>
        </tr>
    	%if o.difference != 0.0:
    		<tr>
        		<td colspan="2">Outstanting Transactions:</td>
			</tr>
	        <tr class="row">
	            <td colspan="2">
	                <table class="tr_bottom_line"></table>
	            </td>
	        </tr>
	        %for p in o.line_ids:
	        	%if not p.checked:
		            <tr>
		                <td> - Ref : ${p.reference}</td>
		                <td class="currency">${p.amount}</td>
		            </tr>
		        %endif    
	        %endfor
	        <tr class="row">
	            <td colspan="2">
	                <table class="tr_bottom_line"></table>
	            </td>
	        </tr>
	        <tr>
                <td>&nbsp;</td>
                <td class="currency"><b>${formatLang(o.uncleared_receipts - o.difference)}</b></td>
            </tr>
        %endif 
    </table>
    %if o != objects[-1]:
    	<div style="page-break-after:always;"></div>
    %endif
%endfor
</body>
</html>
