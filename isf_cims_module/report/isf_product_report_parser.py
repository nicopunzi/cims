# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = False

class isf_product_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.product.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_product_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
        })
        self.context = context
        self.result_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_product_report_object(self, ids):
        pr_obj = self.pool.get('isf.product.report')
        
        pr = bs_obj.browse(self.cr, self.uid, ids)
        
        return pr
        
    def _get_product_categories(self, cr, uid, ids=None, sel_ids=[],done=False):
        if done:
            return sel_ids;
            
        cat_obj = self.pool.get('product.category')
        cat_ids = cat_obj.search(cr, uid, [('parent_id','in',ids)])
        if _debug:
            _logger.debug('IDS : %s', ids)
            _logger.debug('CAT IDS : %s', cat_ids)
        if len(cat_ids) == 0:
            done = True
        else:
            for cat in cat_ids:
                if cat not in sel_ids:
                    sel_ids.append(cat)
                    
        return self._get_product_categories(cr, uid, cat_ids,sel_ids, done)
        
    def _get_cost_price(self, cr, uid, product_id):
        lot_o = self.pool.get('stock.production.lot')
        lot_ids = lot_o.search(cr, uid, [('product_id','=',product_id)])
        
        sum_cost = 0.0
        num_lot = 0.0
        for lot in lot_o.browse(cr, uid, lot_ids):
            if lot.cost_price > 0.0:
                sum_cost += lot.cost_price
                num_lot += 1
            
        cost_price = 0.0
        if num_lot > 0:
            cost_price = sum_cost / num_lot
        
        return cost_price

    def _get_allowed_categories(self, cr, uid):
        ir_values = self.pool.get('ir.values')
        product_category_ids = ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'product_category_ids')
        
        return product_category_ids
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.product.report')
        product_obj = self.pool.get('product.product')
        category_obj = self.pool.get('product.category')

        catge_ids = self._get_allowed_categories(self.cr, self.uid)
        
        product_ids = product_obj.search(self.cr, self.uid, [('categ_id','in',catge_ids)])
        for product in product_obj.browse(self.cr, self.uid, product_ids):
            cost_price = self._get_cost_price(self.cr, self.uid, product.id) 
            if cost_price == 0.0:
                cost_price = product.list_price
            res = {
                'code' : product.default_code,
                'name' : product.name,
                'sell_price' : product.list_price,
                'cost_price' : cost_price,
            }
            
            self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.product.webkit', 'isf.product.report', 'addons/isf_cims_module/report/product_report.mako', parser=isf_product_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
