<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    <center>
        <h3>Credit to pay</h3>
    </center>
    %for data in headers():
    <% currency = data['currency'] %>
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 40%;">Customer</th>
                <th class="td-data" style="width: 30%;">Date Start</th>
                <th class="td-data" style="width: 30%;">Date Stop</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 40%;"><b>${data['customer']}</b></th>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_start']}</td>
                <td class="td-data" style="width: 30%;text-align:right;">${data['date_stop']}</td>
            </tr>
        </table>
    </center>
    %endfor
    <br/>
    <center>
        <table class="table-data" width="70%">
            <tr>
                <th class="td-data" style="width: 15%;">Date</th>
                <th class="td-data" style="width: 15%;">TR#</th>
                <th class="td-data" style="width: 15%;">Invoice ID</th>
                <th class="td-data" style="width: 20%;">Due</th>
                <th class="td-data" style="width: 20%;">Discount</th>
                <th class="td-data" style="width: 15%;">Description</th>
            </tr>
            %for data in lines():
                %if data['is_total'] == '0':
                <tr>
                    <td class="td-data" style="width: 15%;">${data['date']}</td>
                    <td class="td-data" style="width: 15%;">${data['tr_id']}</td>
                    <td class="td-data" style="width: 15%;">${data['invoice']}</td>
                    <td class="td-data" style="width: 20%;text-align:right;">${data['total_amount']}</td>
                    <td class="td-data" style="width: 20%;text-align:right;">${data['total_discount']}</td>
                    <td class="td-data" style="width: 15%;">${data['desc']}</td>
                </tr>
                %endif
                %if data['is_total'] == '1':
                    <tr>
                        <td class="td-data-nb" style="width: 30%;" colspan="2"/>
                        <td class="td-data-nb" style="width: 15%;"/><b>Total :</b></td>
                        <td class="td-data-nb" style="width: 20%;text-align:right;"/><b>${data['total_amount']} ${currency}</b></td>
                        <td class="td-data-nb" style="width: 20%;text-align:right;"/><b>${data['total_discount']} ${currency}</b></td>
                        <td class="td-data-nb" style="width: 15%;"/>
                    </tr>
                %endif
            %endfor
        </table>
    </center>
</body>
</html>
