<html>
<head>
    <style type="text/css">
        ${css}
        
@media print {
   thead {display: table-header-group;}
}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print tableheaders()
    print headers()
    print lines()
    """
    %>
    
    <h3>
        Store Reorder Report
    </h3>
    
    <center>
        <table class="table-data" style="width: 33%;">
            %for head in headers():
            <tr>
                <th class="td-data" style="width: 33%;text-align:center;">Date</th>
                <td class="td-data" style="width: 33%;text-align:center;">${head['date']}</td>
            </tr>
            %endfor
        </table>
    </center>

    <br/>

    <table class="table-data">
        <tr>
            <th class="td-data" style="width: 1000px;">Product</th>
            %for thead in tableheaders():
                    <th class="td-data" style="width: 1000px;">${thead['location']}</th>
            %endfor
            <th class="td-data" style="width: 1000px;">Total Qty</th>
            <th class="td-data" style="width: 1000px;">Out of Stock</th>
        </tr>
    
        %for data in lines():
            %if data['first_account'] == '1':
                <tr>
                <td class="td-data" style="width: 1000px;">${data['product']}</td>
            %endif
            %if data['is_total'] == '0':   
            	%if data['warning_level'] == True:
                	<td class="td-data" style="width: 1000px;color:red;text-align:right">${data['qty']}</td>
                %else:
                	<td class="td-data" style="width: 1000px;text-align:right">${data['qty']}</td>
                %endif
            %endif
            %if data['is_total'] == '1':
		        %if data['warning_level'] == True:   
		            <td class="td-data" style="width: 1000px;color:red;text-align:right"><b>${data['qty']}</b></td>
		        %else:
		        	<td class="td-data" style="width: 1000px;text-align:right"><b>${data['qty']}</b></td>
		        %endif
            %endif
            %if data['is_total'] == '2':
		        %if data['out_of_stock'] == True:
		            <td class="td-data" style="width: 1000px;text-align:center">Out of Stock (min: ${data['min_qty']})</td>
		        %else:
		        	<td class="td-data" style="width: 1000px;text-align:center">(< ${data['min_qty']})</td>
	            %endif
            %endif
        %endfor
        </tr>
    </table>
        
        
</body>
</html>
