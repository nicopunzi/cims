<html>
<head>
    <style type="text/css">
        ${css}
@media print {
   thead {display: table-header-group;}
}
 
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data-nb {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}

.td-total {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
    font-weight:bold;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print types()
    print headers()
    print lines()
    """
    %>
    
    <h3>
    Stock Ledger Report
    </h3>

    <center>
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 25%;">Product</th>
                <th class="td-data" style="width: 25%;">Location</th>
                <th class="td-data" style="width: 25%;">Date Start</th>
                <th class="td-data" style="width: 25%;">Date End</th>
            </tr>
            %for head in headers():
                <tr >
                    <td class="td-data" style="width: 25%;">${head['product']}</td>
                    <td class="td-data" style="width: 25%;">${head['store']}</td>
                    <td class="td-data" style="width: 25%;text-align:right">${head['date_start']}</td>
                    <td class="td-data" style="width: 25%;text-align:right">${head['date_end']}</td>
                    
                </tr>
            %endfor
        </table>
    </center>

    <br/>
    <table class="table-data">
            <tr>
                <th class="td-data" style="width: 10%;">Date</th>
                <th class="td-data" style="width: 7%;">Type</th>
                <th class="td-data" style="width: 8%;">TR#</th>
                <th class="td-data" style="width: 8%;">ID#</th>
                <th class="td-data" style="width: 25%;">Ref#</th>
                <th class="td-data" style="width: 5%;">Batch</th>
                <th class="td-data" style="width: 10%;">Expiry Date</th>
                <th class="td-data" style="width: 10%;">Source/Dest</th>
                <th class="td-data" style="width: 10%;">In</th>
                <th class="td-data" style="width: 10%;">Out</th>
                <th class="td-data" style="width: 10%;">Value</th>
            </tr>
            %for data in lines():
                <tr>
                    <td class="td-data" style="width: 10%;">${data['date']}</td>
                    <td class="td-data" style="width: 7%;">${data['type']}</td>
                    <td class="td-data" style="width: 8%;">${data['tr_num']}</td>
                    <td class="td-data" style="width: 8%;">${data['id_num']}</td>
                    <td class="td-data" style="width: 25%;">${data['ref']}</td>
                    <td class="td-data" style="width: 5%;">${data['batch']}</td>
                    <td class="td-data" style="width: 10%;">${data['expiry_date']}</td>
                    <td class="td-data" style="width: 10%;">${data['store']}</td>
                    <td class="td-data" style="width: 10%;text-align:right">${data['in_qty']}</td>
                    <td class="td-data" style="width: 10%;text-align:right">${data['out_qty']}</td>
                    <td class="td-data" style="width: 10%;text-align:right">${data['price']}</td>
                </tr>
            %endfor
            %for data in totals():
            <tr>
                <td class="td-total" colspan="8" style="width: 10%;text-align:right">Totals</td>
                    <td class="td-total" style="width: 10%;text-align:right">${data['total_in_qty']}</td>
                    <td class="td-total" style="width: 10%;text-align:right">${data['total_out_qty']}</td>
                    <td class="td-total" style="width: 10%;text-align:right"></td>
            </tr>
            %endfor
            %for data in total():
            <tr>
                <td class="td-total" colspan="8" style="width: 10%;text-align:right">Balance</td>
                <td class="td-total" colspan="2" style="width: 10%;text-align:center">${data['balance']}</td>
                <td class="td-total" style="width: 10%;text-align:right">${data['price']}</td>
            </tr>
            %endfor
    </table>
</body>
</html>
