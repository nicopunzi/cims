# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug = False

class isf_product_preparement_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.product.preparement.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_product_preparement_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'headers' : self.headers,
            'lines': self.lines,
        })
        self.context = context
        self.result_acc = []
        self.result_header = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_product_report_object(self, ids):
        pr_obj = self.pool.get('isf.product.preparement.report')
        
        pr = bs_obj.browse(self.cr, self.uid, ids)
        
        return pr
    
    
    def _count_items(self, cr, uid, date):
        pos_obj = self.pool.get('isf.pharmacy.pos')
        pos_ids = pos_obj.search(cr, uid, [('date','=',date)])
        
        result = {}
        for prescript in pos_obj.browse(cr, uid, pos_ids):
            for line in prescript.product_line_ids:
                data = result.get(line.product.name)
                if data is None:
                    data = {'sold':0 , 'foc' : 0 ,'disc':0, 'credit' : 0}
                    result.update({line.product.name:data})
                if prescript.type == 'paid':
                    prev_sold = result.get(line.product.name)['sold']
                    sold = prev_sold + line.amount
                    data.update({'sold':sold})
                elif prescript.type == 'foc':
                    prev_foc = result.get(line.product.name)['foc']
                    foc = prev_foc + line.amount
                    data.update({'foc':foc})
                elif prescript.type == 'disc':
                    prev_disc = result.get(line.product.name)['disc']
                    disc = prev_disc + line.amount
                    data.update({'disc':disc})
                elif prescript.type == 'credit':
                    prev_disc = result.get(line.product.name)['credit']
                    disc = prev_disc + line.amount
                    data.update({'credit':disc})
                
                result.update({line.product.name:data})
                
        return result
                    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.product.preparement.report')
        product_obj = self.pool.get('product.product')
        date_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_obj[0]['date']
        
        res = {
            'date' : date,
        }
        
        self.result_header.append(res)
        return self.result_header
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.product.preparement.report')
        product_obj = self.pool.get('product.product')
        date_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_obj[0]['date']
        
        result = self._count_items(self.cr, self.uid, date)
        list_keys = result.keys()
        list_keys.sort()
        for product in list_keys:
            data = result.get(product)
            res = {
                'name' : product,
                'sold' : data['sold'],
                'foc' : data['foc'],
                'discount' : data['disc'],
                'credit' : data['credit'],
                'total' : data['sold'] + data['foc'] + data['disc'] + data['credit']
            }
            
            self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.product.preparement.webkit', 'isf.product.preparement.report', 'addons/isf_cims_module/report/procurement.mako', parser=isf_product_preparement_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
