from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=False


# class stock_move_split_lines_exist(osv.osv_memory):
#     _inherit = 'stock.move.split.lines'
#     
#     _columns = {
#         'expiry_date' : fields.date('Expiry Date'),
#     }
#     
# stock_move_split_lines_exist()

class stock_inventory_line_split(osv.osv_memory):
    _inherit = 'stock.inventory.line.split'
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(stock_inventory_line_split, self).default_get(cr, uid, fields, context=context)
        res['use_exist'] = True
        
        line_id = context.get('active_id')
        if line_id:
            line_o = self.pool.get('stock.inventory.line')
            line_ids = line_o.search(cr, uid, [('id','=',line_id)])
            line = line_o.browse(cr, uid, line_ids)[0]
            
            split_o = self.pool.get('stock.inventory.line.split.lines')
            split_id = split_o.create(cr, uid, {
                'prodlot_id' : line.prod_lot_id.id,
                'quantity' : line.product_qty,
                'expiry_date' : line.prod_lot_id.life_date,
            }, context=context)
            
            res['line_exist_ids'] = [split_id]
        
        return res
        
stock_inventory_line_split()

