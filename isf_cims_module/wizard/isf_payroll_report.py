from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_payroll_report(osv.osv_memory):
    _name = "isf.payroll.payslip"
    _description = "Payroll Report Wizard"
    
    def _get_grade_selection(self, cr, uid, context=None):
        hr_contract_type_obj = self.pool.get('hr.contract.type')
        hr_contract_type_ids = hr_contract_type_obj.search(cr, uid, [])
        
        result = [('all','All')]
        for hr_contract_type in hr_contract_type_obj.browse(cr, uid, hr_contract_type_ids):
            result.append((hr_contract_type.id,hr_contract_type.name))
            
        return result
    
    _columns = {
        'payslip_batch_id': fields.many2one('hr.payslip.run', 'Payslip', select=True,required=True),
        'grade_ids' : fields.selection(_get_grade_selection,'Grade', required=True),
    }
    
    _dafaults = {
        'grade_ids' : 'all',
    }
        
    
isf_payroll_report()
