from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_pharmacy_credit_wizard(osv.osv_memory):
    _name = "isf.pharmacy.credit.wizard"
    _description = "Pharmacy Credit Report"
    _columns = {
        'date_start' : fields.date('Date Start', required=True),
        'date_stop' : fields.date('Date End', required=True),
        'customer_id' : fields.many2one('res.partner','Customer',required=True)
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_stop' : fields.date.context_today,
    }
    
    
isf_pharmacy_credit_wizard()
