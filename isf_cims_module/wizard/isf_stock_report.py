from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_move_report(osv.osv_memory):
    _name = "isf.stock.move.report"
    _description = "Stock Report"
    _columns = {
        'date_start' : fields.date('Date Start',required=True),
        'date_end' : fields.date('Date End',required=True),
        'location_id' : fields.many2one('stock.location',string="Stock Location", required=True),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
    }
    
    
isf_stock_move_report()
