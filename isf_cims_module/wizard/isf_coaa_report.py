# -*- coding: utf-8 -*-
from openerp.osv import orm, fields
from openerp import netsvc

class isf_coaa_account(orm.TransientModel):
    def _get_default_account_id(self, cr, uid, context=None):
        return self.pool.get('account.analytic.account').search(cr, uid, [], context=context, limit=1)

    _name = "isf.coaa.account"
    _description = "COAA Account"
    _columns = {
        'account': fields.many2one('account.analytic.account', "Analytic Account", select=True)
    }
    #_defaults = {
    #    'account': _get_default_account_id
    #}


isf_coaa_account()
