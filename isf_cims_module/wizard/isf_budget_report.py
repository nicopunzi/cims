from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_account_budget_report(osv.osv_memory):
    _name = "isf.budget.report"
    _description = "Budget Report Wizard"
    
    _columns = {
        'begin_date' : fields.many2one('account.period','Begin Period', required=True),
        'end_date' : fields.many2one('account.period','End Period', required=True),
        'budget_ids': fields.many2one('crossovered.budget', string="Budget", select=True,required=True),
    }
    
isf_account_budget_report()
