# -*- coding: utf-8 -*-
from openerp.osv import orm, fields, osv
from openerp import netsvc

class isf_coa_account(osv.osv_memory):
    def _get_default_account_id(self, cr, uid, context=None):
        return self.pool.get('account.account').search(cr, uid, [], context=context, limit=1)

    _name = "isf.coa.account"
    _description = "COA Account"
    _columns = {
        'account': fields.many2one('account.account', "Account", select=True)
    }
    #_defaults = {
    #    'account': _get_default_account_id
    #}

isf_coa_account()
