from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_pharmacy_daily_income_wizard(osv.osv_memory):
    _name = "isf.pharmacy.daily.income.wizard"
    _description = "Pharmacy Daily Income Report"
    _columns = {
        'date' : fields.date('Date', required=True),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
    }
    
    
isf_pharmacy_daily_income_wizard()
