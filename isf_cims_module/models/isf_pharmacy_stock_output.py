from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False


class isf_pharmacy_stock_output_line_select(osv.TransientModel):
    _name = 'isf.pharmacy.stock.output.line.select'
    
    _columns = {
        'product_ids' : fields.many2many('stock.production.lot','id','id',relation='isf_pharmacy_stock_output_lot_rel',string="Products",required=False,domain=[('stock_available','>',0)]),
        'location_id' : fields.many2one('stock.location','Source Location',required=False),
    }
    
    def get_stock_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_pharmacy_stock_output_line_select, self).default_get(cr, uid, fields, context=context)
            
        statement_id = context.get('statement_id')
        data_o = self.pool.get('isf.pharmacy.stock.output')
        data_ids = data_o.search(cr, uid, [('id','=',statement_id)])
        data = data_o.browse(cr, uid, data_ids)[0]
        res['location_id'] = self.get_stock_location_id(cr, uid)
        
        if _debug:
            _logger.debug('RES : %s', res)
        
        return res
    
    def import_product(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        
        if context is None:
            context = {}
        ctx = {}
        ctx = context.copy()
        ctx.update({
            'location_id' : data.location_id.id
        })
    
        line_obj = self.pool.get('isf.pharmacy.stock.output.line')
        stock_o = self.pool.get('stock.production.lot')
        stock_ids = []
        for stock in data['product_ids']:
            stock_ids.append(stock.id)
            
        active_id = context.get('active_id')
        state = context.get('state')
        
        
        now = datetime.datetime.now()
        
        for stock in stock_o.browse(cr, uid, stock_ids, context=ctx):
            expired = False
            exp = datetime.datetime.strptime(stock.ref, "%Y-%m-%d")
            if exp < now:
                expired = True
                
            if stock.stock_available > 0.0:
                line_id = line_obj.create(cr, uid, {
                    'stock_id' : stock.id,
                    'expiry_date' : stock.ref,
                    'product_id' : stock.product_id.id,
                    'stock_available' : stock.stock_available,
                    'quantity' : 1,
                    'uom_id' : stock.product_id.uom_id.id,
                    'expired' : expired,
                    'statement_id' : active_id,
                    'state' : state,
                }, context=context)
                
        return True



class isf_pharmacy_stock_output_line(osv.Model):
    _name = 'isf.pharmacy.stock.output.line'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Lot Id'),
        'expiry_date' : fields.char('Expiry Date'),
        'product_id' : fields.many2one('product.product','Product'),
        'stock_available' : fields.integer('Available'),
        'quantity' : fields.integer('Quantity'),
        'uom_id' : fields.many2one('product.uom','UoM'),
        'expired' : fields.boolean('Expired'),
        'statement_id' : fields.many2one('isf.pharmacy.stock.output',string="Statement Id",select=True),
        'move_id' : fields.many2one('stock.move','id'),
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
    }
    
    _defaults = {
        'state' : 'draft',
    }

class isf_pharmacy_stock_output(osv.Model):
    _name = 'isf.pharmacy.stock.output'
    
    def _get_output_type(self, cr, uid,  context=None):
        return [('expired','Expired'),('broken','Broken\Loss')]
        
    
    _columns = {
        'state' : fields.selection([('draft','Draft'),('done','Done')],'State',readonly=True),
        'name' : fields.char('Description', size=128, required=True),
        'date' : fields.date('Date',required=True),
        'output_type' : fields.selection(_get_output_type,'Expense Type', required=True),
        'line_ids' : fields.one2many('isf.pharmacy.stock.output.line','statement_id','Products'),
        'move_cost_id' : fields.many2one('account.move','id'),
    }
    
    _defaults = {
        'state' : 'draft',
    }
    
        
    def get_expired_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','expired_location_id')
        
    def get_broken_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','broken_location_id')
        
    def get_expired_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_expired_account_id')
        
    def get_broken_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','cost_of_item_broken_account_id')
    
    def get_asset_account_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_account_id')
    
    def get_journal_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_journal_id')
        
    def get_aa_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','analytic_account_id')
    
    def get_stock_location_id(self, cr, uid, context=None):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','stock_location_id')
    
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pharmacy.pos','enable_stock')
    
    def confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cr, uid, ids)[0]
     
        expense_account_id = False
        journal_id = False
        aa_id = False
        
        isf_lib = self.pool.get('isf.lib.utils')
        stock_enabled = self._get_enable_stock(cr, uid)
                
        if wizard.output_type == 'expired':
            expense_account_id = self.get_expired_account_id(cr, uid)
            location_dest_id = self.get_expired_location_id(cr, uid)
        elif wizard.output_type == 'broken':
            expense_account_id = self.get_broken_account_id(cr, uid)
            location_dest_id = self.get_broken_location_id(cr, uid)
            
            
        account_asset_id = self.get_asset_account_id(cr, uid)
        journal_id = self.get_journal_id(cr, uid)
        aa_id = self.get_aa_id(cr, uid)
        
        line_o = self.pool.get('isf.pharmacy.stock.output.line')
        location_source_id = self.get_stock_location_id(cr, uid)
        
        if stock_enabled:
            move_pool = self.pool.get('account.move')
            move_line_pool = self.pool.get('account.move.line')		
            move_cost = move_pool.account_move_prepare(cr, uid, journal_id, wizard.date, ref=wizard.name, context=context)	
            move_cost_id = move_pool.create(cr, uid, move_cost, context=context)
        
        if _debug:
            _logger.debug('Account Expense : %s',expense_account_id)
            _logger.debug('Account Asset : %s',account_asset_id)
            _logger.debug('Account AA : %s',aa_id)
        
        for line in wizard.line_ids:
            cost_price = line.stock_id.cost_price * line.quantity
            if cost_price == 0.0:
                cost_price = line.product_id.list_price * line.quantity
                
            move_id = isf_lib._generate_stock_move(cr, uid, wizard.name, location_source_id,location_dest_id, line.product_id, line.quantity,  wizard.date, 'internal',line.stock_id.id,str(wizard.id),line.uom_id,context=context)
            line_o.write(cr, uid, line.id, {'move_id' : move_id, 'state' : 'done'}, context=context)
            if stock_enabled:
                isf_lib._generate_cost_of_items_moves(cr, uid, move_line_pool, wizard.name, line.product_id.name, cost_price, move_cost_id, expense_account_id,account_asset_id, aa_id, aa_id, context=None)
            
        if stock_enabled:
            self.write(cr, uid, wizard.id, {'move_cost_id' : move_cost_id}, context=context)
            
        self.write(cr, uid, ids, {'state' : 'done' }, context=context)
        
        return True
        
    def set_to_draft(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        stock_move_o = self.pool.get('stock.move')
        move_o = self.pool.get('account.move')
        line_o = self.pool.get('isf.pharmacy.stock.output.line')
        
        #
        # Remove stock moves
        #
        for line in data.line_ids:
            stock_move_o.write(cr, uid, line.move_id.id, {'state' : 'cancel'}, context=context)
            line_o.write(cr, uid, line.id, {'state' : 'draft'}, context=context)
            
        #
        # Remove journal entry (cost of items move)
        #
        move_ids = []
        if data.move_cost_id:
            move_ids.append(data.move_cost_id.id)
        move_o.unlink( cr, uid, move_ids, context=None, check=False)
            
        self.write(cr, uid, ids, {'state' : 'draft'}, context=context)
        return True
    
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        stock_moves = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in stock_moves:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a stock output entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
