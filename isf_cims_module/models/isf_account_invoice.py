from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time

_logger = logging.getLogger(__name__)
_debug=False


class account_invoice(osv.osv):
    _inherit = 'account.invoice'
	
    def _get_partner(self, cr, uid, context=None):
    
        res_pool = self.pool.get('res.partner')
        res_ids = res_pool.search(cr, uid, [('customer','=',True)], limit=1)
        res_obj = res_pool.browse(cr, uid, res_ids, context=context)
        
        if _debug:
            _logger.debug('Context : %s',context)
            
        
        for partner in res_obj:
            if _debug:
                _logger.debug('Customer : %d',partner.id)
        return partner.id
    
    def _get_date(self, cr, uid, context=None):
        date = datetime.datetime.now()
        
        return date.strftime("%Y-%m-%d")
    
    _columns = {
        'document_number': fields.char('Document Number', size=64, required=False, states={'draft':[('readonly',False)]}),
        'date_invoice' : fields.date('Date', required=False),  
    }
    
    _defaults = {
        'date_invoice' : _get_date,
    }
        
    
    def onchange_document_number(self, cr, uid, ids, doc_number, context=None):
        vals = {'value':{} }
       
        vals['value'].update({
            'name' : doc_number,
        })
        return vals
        
    def onchange_partner_id(self, cr, uid, ids, type, partner_id,\
            date_invoice=False, payment_term=False, partner_bank_id=False, company_id=False):
        result = super(account_invoice, self).onchange_partner_id(cr, uid, ids, type, partner_id,date_invoice, payment_term, partner_bank_id, company_id)    
        account_id = result['value'].get('account_id')
        
        if account_id:
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]
            
            if account.currency_id.id:
                currency_id = account.currency_id.id,
            else:
                isf_lib = self.pool.get('isf.lib.utils')
                currency_id = isf_lib.get_company_currency_id(cr, uid)
            
            result['value'].update({
                'currency_id' : currency_id,
            })
        return result
	
account_invoice()

# class account_invoice_line(osv.osv):
#     _inherit = 'account.invoice.line'
# 	
#     _columns = {
#         'temp_amount' : fields.float('Amount', store=False),
#         'temp_amount_view' : fields.float('Amount', store=False),
#     }
#     
#     
#     def onchange_price_quantity(self, cr, uid, ids, quantity, price, context=None):
#         vals = {'value':{} }
# 			
#         temp_amount = price * quantity
# 		
#         if _debug:
#             _logger.debug('onchange_price_quantity : Price(%f) Quantity(%f) Amount(%f)',price, quantity,temp_amount)
# 		
# 		
#         vals['value'].update({
#             'temp_amount_view' : temp_amount,
#             'temp_amount' : temp_amount,
#         })
# 		
#         return vals
#     
# account_invoice_line()


class account_voucher(osv.osv):
    _inherit = 'account.voucher'

    def _get_invoice_number(self, cr, uid, context=None):
        if _debug:
            _logger.debug('_get_invoice_number : %s', context)
       
        invoice_id = context.get('invoice_id')
        invoice_pool = self.pool.get('account.invoice')
        invoice_ids = invoice_pool.search(cr, uid , [('id','=',invoice_id)],limit=1)
        invoice_obj = invoice_pool.browse(cr, uid, invoice_ids, context=context)
       
        for invoice in invoice_obj:
            return invoice.number
    
    def _get_original_amount(self, cr, uid, context=None):
        invoice_id = context.get('invoice_id')
        invoice_pool = self.pool.get('account.invoice')
        invoice_ids = invoice_pool.search(cr, uid , [('id','=',invoice_id)],limit=1)
        invoice_obj = invoice_pool.browse(cr, uid, invoice_ids, context=context)
       
        for invoice in invoice_obj:
            return invoice.amount_total
    
    _columns  ={
        'original_amount' : fields.float('Original Amount'),
    }
    
    _defaults = {
        'name' : _get_invoice_number,
        'original_amount' : _get_original_amount,
    }
    
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount, ttype, company_id, context=None):
        if context is None:
            context = {}
        if not journal_id:
            return False
        journal_pool = self.pool.get('account.journal')
        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        account_id = journal.default_credit_account_id or journal.default_debit_account_id
        tax_id = False
        if account_id and account_id.tax_ids:
            tax_id = account_id.tax_ids[0].id

        vals = {'value':{} }
        if ttype in ('sale', 'purchase'):
            vals = self.onchange_price(cr, uid, ids, line_ids, tax_id, partner_id, context) 
            vals['value'].update({'tax_id':tax_id,'amount': amount})
        currency_id = False
        if journal.currency:
            currency_id = journal.currency.id
        else:
            currency_id = journal.company_id.currency_id.id
        vals['value'].update({'currency_id': currency_id})
		
        company_currency_id = self._get_company_currency_id(cr, uid, context=context)
        
        expected_currency_id = context.get('payment_expected_currency')
        currency_pool = self.pool.get('res.currency')
        amount_currency = None
        
        if _debug:
            _logger.debug('Expected : %d , Currency_id : %d',expected_currency_id,currency_id)
        
        if expected_currency_id != currency_id: 
            amount_currency = currency_pool.compute(cr, uid, expected_currency_id, currency_id, amount, context=context)
        else:
            amount_currency = self._get_original_amount(cr, uid, context=context)
            
        res = self.onchange_partner_id(cr, uid, ids, partner_id, journal_id, amount_currency, currency_id, ttype, date, context)
        for key in res.keys():
            vals[key].update(res[key])
            
        vals['value'].update({
            'amount' : amount_currency,
        })
        return vals

account_voucher()