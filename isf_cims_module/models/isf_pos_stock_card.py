from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging
import datetime
import openerp
from lxml import etree
import re
import datetime

_logger = logging.getLogger(__name__)
_debug=False

class isf_pos_stock_card_line(osv.TransientModel):
    _name = 'isf.pos.stock.card.view.line'
    
    _columns = {
        'date' : fields.date('Date'),
        'in_stock' : fields.integer('In'),
        'out_stock' : fields.integer('Out'),
        'line_id': fields.many2one('isf.stock.card', 'Line', select=True),
        'older_entry' : fields.boolean('Older Entry'),
        'balance' : fields.integer('Balance'),
        'note' : fields.char('Note',size=64),
    }
    
    _defaults = {
        'date' :  fields.date.context_today,
        'older_entry' : False,
    }
    
    


class isf_pos_stock_card(osv.TransientModel):
    _name = 'isf.pos.stock.card'
    
    _columns = {
        'product_id' : fields.many2one('product.product','name','Product'),
        'date_start': fields.date('Date Start', required=False),
        'date_end' : fields.date('Date End', required=False),
        'line_ids' : fields.one2many('isf.stock.card.view.line', 'line_id', 'Stock Line'),
        'location_id' : fields.many2one('stock.location','Location', required=True,domain=[('available_in_selection','=',True)]),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
    }
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(isf_pos_stock_card, self).default_get(cr, uid, fields, context=context)
        product_id = context.get('product_id')
        if product_id is not None:
            res['product_id'] = product_id
            stock_move = self._load_stock_move(cr, uid, 0, product_id)
            line_ids = []
            line_obj = self.pool.get('isf.pos.stock.card.view.line')
            for move in stock_move:
                line_id = line_obj.create(cr, uid, move, context=context)
                line_ids.append(line_id)
            res['line_ids'] = line_ids
            
        return res
            
    def _load_stock_move(self, cr, uid, ids, product_id, date_start, date_end, location_id, context=None):
        if context is None:
            context = {}
        
        StartDate = date_start + ' 00:00:01'
        EndDate = date_end+' 23:59:59'
        stock_card_move_ids = []
        stock_move_obj = self.pool.get('stock.move')
        stock_move_in_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id.id','=',location_id),('state','=','done'),('date_expected','>=',StartDate),('date_expected','<=', EndDate)],order='date_expected')
        stock_move_out_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_id.id','=',location_id),('state','=','done'),('date_expected','>=',StartDate),('date_expected','<=', EndDate)],order='date_expected')
        
        res = {'value':{}}
        balance = 0
        in_qty = 0
        out_qty = 0
        # Load Opening balance
        StartDate = date_start + ' 00:00:00'
        stock_ob_in_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id.id','=',location_id),('state','=','done'),('date_expected','<',StartDate)],order='date_expected')
        stock_ob_out_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_id.id','=',location_id),('state','=','done'),('date_expected','<',StartDate)],order='date_expected')
        
        for stock_move in stock_move_obj.browse(cr, uid, stock_ob_in_ids):
            factor = stock_move.product_uom.factor
            in_qty += (stock_move.product_qty / factor)
            
        for stock_move in stock_move_obj.browse(cr, uid, stock_ob_out_ids):
            factor = (stock_move.product_uos.factor)
            out_qty += stock_move.product_uos_qty / factor
        
        o_balance = in_qty - out_qty
        date_ob_tmp = datetime.datetime.strptime(date_start, '%Y-%m-%d')
        date_ob = date_ob_tmp - datetime.timedelta(days=1)
        date_ob_str = date_ob.strftime("%Y-%m-%d")
        
        move = {'is_ob':True, 'in_qty':0 , 'out_qty' : 0 ,'balance' : o_balance, 'note': 'Opening Balance'}
        res['value'].update({date_ob_str:move})
        
        for stock_move in stock_move_obj.browse(cr, uid, stock_move_in_ids):
            # Group by date
            date = stock_move.date_expected[:10]
            move = res['value'].get(date)
            if  move is None:
                move = {'is_ob':False,'in_qty':0 , 'out_qty' : 0 ,'note': False,'balance' :0}
                res['value'].update({date:move})
            
            #move['balance'] += stock_move.product_qty
            factor = stock_move.product_uom.factor
            in_qty = stock_move.product_qty / factor
            move['in_qty'] += in_qty
            if stock_move.name.find('INV:') != -1:
                if not move['note']:
                    move.update({'note':stock_move.name.strip('')})
            #move.update({'in_qty': qty, 'balance':balance})
            res['value'].update({ date: move})
        
        for stock_move in stock_move_obj.browse(cr, uid, stock_move_out_ids):
             # Group by date
            date = stock_move.date_expected[:10]
            move = res['value'].get(date)
            if  move is None:
                move = {'is_ob':False,'in_qty':0 , 'out_qty' : 0 ,'note': False,'balance' :0}
                res['value'].update({date:move})
            
            #move['balance'] -= stock_move.product_qty
            factor = (stock_move.product_uos.factor)
            out_qty = stock_move.product_uos_qty / factor
            move['out_qty'] += out_qty
            if "INV:" in stock_move.name:
                if not move['note']:
                    move.update({'note':stock_move.name.strip('')})
            #move.update({'out_qty': qty, 'balance':balance})
            res['value'].update({ date: move})
                
            
        list_keys = res['value'].keys()
        list_keys.sort()
        balance = o_balance
        for date_key in list_keys:
            move = res['value'].get(date_key)
            if move['is_ob']:
                balance = move['balance']
            else:
                balance += (move['in_qty'] - move['out_qty'])
                
            stock_card_move = {
                'date' : date_key,
                'in_stock' : move['in_qty'],
                'out_stock' : move['out_qty'],
                'balance' : balance,
                'older_entry' : True,
                'note' : move['note'],  
            }
            stock_card_move_ids.append(stock_card_move)
        return stock_card_move_ids
    
    def onchange_product(self, cr, uid, ids, product_id, date_start, date_end, location_id, context=None):
        result = {'value' : {}}
        if not location_id:
            return result
        
        line_ids = []
        line_obj = self.pool.get('isf.stock.card.view.line')
        
        stock_card_move = self._load_stock_move(cr, uid, ids, product_id, date_start, date_end, location_id, context=context)
        for move in stock_card_move:
            line_id = line_obj.create(cr, uid, move, context=context)
            line_ids.append(line_id)
        
        
        result['value'].update({
            'line_ids' : line_ids
        })
        return result