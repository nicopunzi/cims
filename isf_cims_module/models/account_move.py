from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time

_debug=False
_logger = logging.getLogger(__name__)

class isf_journal_entries_multiple_post(osv.osv_memory):
    _name = 'isf.journal.entries.multiple.post'
    
    def multiple_post(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('account.move')
        active_ids = context['active_ids']
        
        move_obj.post(cr, uid, active_ids, context=context)
            
        return True
        
#     def cancel(self, cr, uid, ids, context=None):
#         return {'type': 'ir.actions.act_window_close'}
        
isf_journal_entries_multiple_post()
        
        