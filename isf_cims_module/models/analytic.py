from openerp.osv import fields, osv
from tools.translate import _
import logging
import re

_debug=False
_logger = logging.getLogger(__name__)

class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'
    
    # Override name_get with Analytic Account code as prefix
    def name_get(self, cr, uid, ids, context=None):
        res = []
        if not ids:
            return res
        if isinstance(ids, (int, long)):
            ids = [ids]
        for id in ids:
            elmt = self.browse(cr, uid, id, context=context)
            name = _("%s - %s") % (elmt.code, self._get_one_full_name(elmt))
            res.append((id,name))
        return res
    
#     def _get_full_name(self, cr, uid, ids, name=None, args=None, context=None):
#         if context == None:
#             context = {}
#         res = {}
#         for elmt in self.browse(cr, uid, ids, context=context):
#             res[elmt.id] = self._get_one_full_name(elmt)
#         return res
#     
#     def _get_one_full_name(self, elmt, level=6):
#         if level<=0:
#             return '...'
#         if elmt.parent_id and not elmt.type == 'template':
#             parent_path = self._get_one_full_name(elmt.parent_id, level-1) + " / "
#         else:
#             parent_path = ''
#         return parent_path + elmt.name

    # Override name_search will look for all keywords in the whole account full path
    def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=100):
        if not args:
            args=[]
        if context is None:
            context={}
        if name:
            account_ids = self.search(cr, uid, [('code', '=', name)] + args, limit=limit, context=context)
            if not account_ids:
                account_ids = []
                keywords = name.split(' ');
                if _debug:
                    _logger.debug('==> search keywords : %s', keywords)

                account_search_ids = self.search(cr, uid, args, order='code')
                account_browse = self.browse(cr, uid, account_search_ids)
                for account in account_browse:
                    
                    # all keywords must be found in the full path name + account name
                    if all(re.search(name2, self._get_one_full_name(account), re.IGNORECASE) for name2 in keywords):
                        account_ids.append(account.id)
                        if _debug:
                            _logger.debug('==> found in : %s', self._get_one_full_name(account))
        else:
            account_ids = self.search(cr, uid, args, limit=limit, context=context)
        if _debug:
            _logger.debug('==> accounts selected : %s', account_ids)
        return self.name_get(cr, uid, account_ids, context=context)