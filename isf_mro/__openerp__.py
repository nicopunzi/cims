# -*- coding: utf-8 -*-

{
	'name': 'ISF MRO Extension',
	'version': '0.7',
	'category': 'Tools',
	'description': """

ISF MRO Extension 
==================
	
* Add a link to mantainance calendar in mantainance menu
* Customized Asset module
* Asset Type

""",
	'author': 'Matteo Angelino (matteo.angelino@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['mro','asset'],
	'data': [
        'views/isf_mro_view.xml',
        'views/product_view.xml',
        'security/mro_security.xml',
        'security/ir.model.access.csv',
        'data/csv/isf.home.dashboard.action.group.csv',
		'data/csv/isf.home.dashboard.action.csv',
	],
	'css' : [],
	'demo': [],
	'installable' : True,
}

