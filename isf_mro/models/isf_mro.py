from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time

_logger = logging.getLogger(__name__)
_debug=False

class isf_mro_asset_type(osv.Model):
    _name = 'isf.mro.asset.type'
    
    _columns = {
        'name' : fields.char('Type',required=True),
        'parent_type' : fields.many2one('isf.mro.asset.type','Parent'),
    }
    
class asset_asset(osv.osv):
    _inherit = 'asset.asset'
    _name = 'asset.asset'
    
    def _get_asset_usage(self, cr, uid, context=None):
        return (
            ('W', 'Working'),
            ('C', 'To Check'),
            ('NW','Not Working'),
            ('NF','Not Found'),
            ('NU','Not In Use'),
            ('NWP','Not Working Properly'),
            ('N','No'),
            )
    
    def _get_status(self, cr, uid, context=None):
        return (
            ('N','NEW'),
            ('O','OPERABLE'),
            ('G','GOOD'),
            ('U','UNKNOWN'),
            )
    
    def _get_level(self, cr, uid, context=None):
        return (
            ('a','A'),
            ('b','B'),
            ('c','C'),
            ('d','D'),
            ('e','E'),
            )
    
    _columns = {
        'type' : fields.many2one('isf.mro.asset.type','Item'),
        'status' : fields.selection(_get_status,'Status', required=False), 
        'usage' : fields.selection(_get_asset_usage,'Usage', required=False),
        'level' : fields.selection(_get_level,'Maintenance Level', required=False),
        'note' : fields.text('Note',Size=512, required=False),
        'umdns' : fields.char('UM-DNS',size=16,required=False),
        'supply_electrical' : fields.boolean('Electrical',required=False),
        'supply_pneumatic' : fields.boolean('Pneumatic',required=False),
        'supply_water' : fields.boolean('Water',required=False),
        'power' : fields.float('Power',size=6, required=False),
        'user_manual': fields.binary('User Manual'),
        'operating_manual': fields.binary('Operating Manual'),
        'location_id' : fields.many2one('stock.location','Location')
    }
    
asset_asset()

    
class mro_order(osv.Model):
    _inherit = 'mro.order'
    
    
    _columns = {
        #Operations
        'op_cleaning' : fields.boolean('Cleaning'),
        'op_testing' : fields.boolean('Testing'),
        'op_calibrating' : fields.boolean('Calibrating'),
        'op_part_sub' : fields.boolean('Part Substitution'),
        'op_security_check' : fields.boolean('Security Check'),
        'op_restore' : fields.boolean('Restore'),
        'op_disposing' : fields.boolean('Disposing'),
        'op_call_for_service' : fields.boolean('Call For Service'),
        # Docs
        'mantainance_check_list': fields.binary('Mantainance Check List'),
        'other_docs': fields.binary('Others Documentation'),
        # HR
        'employee_ids' : fields.many2many('hr.employee',String='Employee'),
    }
    
class mro_request(osv.Model):
    _inherit = 'mro.request'
    
    _columns = {
        'department_id' : fields.many2one('hr.department','Department'),
        'request_employee_id' : fields.many2one('hr.employee','Requestor'),
        'priority' : fields.selection([('low','Low'),('medium','Medium'),('high','High')],'Priority'),
        'type' : fields.selection([('prev','Preventive'),('corr','Corrective'),('new','New Installation'),('disp','Disposal')],'Type'),
    }
    
    _defaults = {
        'priority' : 'low',
        'type' : 'prev',
    }
    