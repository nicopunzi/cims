# -*- coding: utf-8 -*-
{
	'name': 'Export To Excel',
	'version': '0.1',
	'category': 'Tools',
	'author': 'Antonio Verni (antonio.verni@informaticisenzafrontiere.org)',
    'website': 'www.informaticisenzafrontiere.org',
	'description': """
Export Webkit reports to Excel
==============================

Usage: add context="{'xls_output':True}" to any button linked to a webkit report

Example: 

<button name="%(isf_tosa_treasury_report)d" string="Print" type="action" class="oe_highlight"/>
<button name="%(isf_tosa_treasury_report)d" string="Excel" type="action" class="oe_highlight" context="{'xls_output':True}"/>


""",
    'depends': [],
    'data': [],
    'installable' : True,
    'demo': [],
}
